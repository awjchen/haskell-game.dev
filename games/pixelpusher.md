---
tags: [game, 2d, gl, glfw, osx, linux, windows, multiplayer]
---

# Pixelpusher

> Control a small swarm of drones and fly them into your opponents while dodging theirs.
> Capture and hold control points with your team to win.

- Itch: <https://aetup.itch.io/pixelpusher>
- Source: <https://gitlab.com/awjchen/pixelpusher>

![2020-11-29](https://img.itch.zone/aW1hZ2UvODE3Nzg0LzQ1ODc0NTAucG5n/original/ufRAZx.png)
