---
tags: [game, 2d, sdl2, sdl2-mixer, sdl2-image, yampa, linux, windows]
---

# Peoplemon

- Itch: <https://linearity.itch.io/peoplemon>
- Source: <https://hub.darcs.net/linearity/pplmonad>
- Announce: <https://reddit.com/r/haskell/comments/c29lks/peoplemon_an_allhaskell_roleplaying_game/>

![2020-11-29](https://img.itch.zone/aW1nLzIyMDAyMDMucG5n/original/HAH2lf.png)
