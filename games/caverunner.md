---
tags: [game, 2d, ansi-terminal-game, osx, linux, windows]
---

# Caverunner

```

  _________ __   _____  _______  ______  ____  ___  _____
 / ___/ __ `/ | / / _ \/ ___/ / / / __ \/ __ \/ _ \/ ___/
/ /__/ /_/ /| |/ /  __/ /  / /_/ / / / / / / /  __/ /
\___/\__,_/ |___/\___/_/   \__,_/_/ /_/_/ /_/\___/_/

```

> A simple one-file cross-platform terminal game in Haskell, built with ansi-terminal-game and stack.

- Source: <https://github.com/simonmichael/games/tree/main/caverunner>

![2021-12-20](https://github.com/simonmichael/games/raw/main/caverunner/caverunner.anim.gif)
