{ siteTitle = "Haskell GameDev"
, author = Some "dpwiz"
, siteBaseUrl = Some "https://haskell-game.dev"
-- List of themes: https://neuron.zettel.page/2014601.html
, theme = "purple"
, editUrl = Some "https://gitlab.com/dpwiz/haskell-game.dev/-/blob/master/"
, mathJaxSupport = True
}
