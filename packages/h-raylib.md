---
tags: [package, raylib]
---

# h-raylib

Haskell bindings to the Raylib library.

- Hackage: <https://hackage.haskell.org/package/h-raylib>
- Source: <https://github.com/Anut-py/h-raylib>
- Examples: <https://github.com/Anut-py/h-raylib/tree/master/examples>

## Upstream

<https://www.raylib.com/>

> raylib is a programming library to enjoy videogames programming; no fancy interface, no visual helpers, no gui tools or editors... just coding in pure spartan-programmers way.

## Minimal example

```haskell
module Main where

import Raylib.Core
  ( beginDrawing,
    clearBackground,
    closeWindow,
    endDrawing,
    initWindow,
    setTargetFPS,
  )
import Raylib.Core.Text (drawText)
import Raylib.Util (whileWindowOpen0)
import Raylib.Util.Colors (lightGray, rayWhite)

main :: IO ()
main = do
  initWindow 600 450 "raylib [core] example - basic window"
  setTargetFPS 60

  whileWindowOpen0
    ( do
        beginDrawing

        clearBackground rayWhite
        drawText "Basic raylib window" 30 40 18 lightGray

        endDrawing
    )

  closeWindow
```

```query
tag: raylib
```
