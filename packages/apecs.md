---
tags: [package, apecs, ecs]
---

# Apecs

- Hackage: <https://hackage.haskell.org/package/apecs>
- Source: <https://github.com/jonascarpay/apecs>
- Tutorial: <https://github.com/jonascarpay/apecs/blob/master/examples/Shmup.md>

> apecs is a fast, type-driven Entity-Component-System library for game programming.

## Minimal example

```haskell
import Apecs
import Linear (V2 (..))

newtype Position = Position (V2 Double) deriving Show
newtype Velocity = Velocity (V2 Double) deriving Show
data Flying = Flying

makeWorldAndComponents "World" [''Position, ''Velocity, ''Flying]

game :: System World ()
game = do
  newEntity (Position 0, Velocity 1)
  newEntity (Position 2, Velocity 1)
  newEntity (Position 1, Velocity 2, Flying)

  -- 1. Add velocity to position
  -- 2. Apply gravity to non-flying entities
  -- 3. Print a list of entities and their positions
  cmap $ \(Position p, Velocity v) -> Position (v+p)
  cmap $ \(Velocity v, _ :: Not Flying) -> Velocity (v - V2 0 1)
  cmapM_ $ \(Position p, Entity e) -> liftIO . print $ (e, p)

main :: IO ()
main = initWorld >>= runSystem game
```

## Extras

- [apecs-stm](https://hackage.haskell.org/package/apecs-stm): Stores running in STM.
- [apecs-gloss](https://hackage.haskell.org/package/apecs-gloss): Simple 2D gloss-based rendering.

---

```query
tag: apecs
```
